export class Survey {
   constructor(
       public surveyTitle: string,
       public surveyQuestions: SurveyQuestion,
   ) {} 
}

export class SurveyQuestion {
    constructor(
        public questionTitle: string,
        public questionType: string,
        public questionGroup: QuestionGroup,
    ) {}
}

export class QuestionGroup{
    constructor(
        public options: Array<any>
    ) {}
}