import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', 
    loadChildren: () => import('./create-survey/create-survey.module').then(m => m.CreateSurveyModule)
  },
  {
    path: '', 
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
