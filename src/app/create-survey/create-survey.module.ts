import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule } from '@angular/material/input';
import { CreateSurveyComponent } from './create-survey.component' 

import { CreateSurveyRoutingModule } from './create-survey-routing.module';


@NgModule({
  declarations: [
    CreateSurveyComponent,
  ],
  imports: [
    CommonModule,
    CreateSurveyRoutingModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatCardModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatInputModule
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule
  ]
})
export class CreateSurveyModule { }
