import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Survey, Option, QuestionType } from './models';
@Component({
  selector: 'app-create-survey',
  templateUrl: './create-survey.component.html',
  styleUrls: ['./create-survey.component.scss'],
})
export class CreateSurveyComponent implements OnInit {
  @Input('input') input: Text;
  @Input() form: FormGroup;
	
  constructor() {}

  surveyForm: FormGroup;
  selectedOption = [];
  selectFile;
  jsonObject: {};

  questions: QuestionType[] = [
    { value: 'Radio', viewValue: 'Radio - Single answer' },
    { value: 'Checkbox', viewValue: 'Checkbox - Multi answer' },
    { value: 'Text', viewValue: 'Text' },
  ];

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    let surveyTitle = '';
    let surveyQuestions = new FormArray([]);
    this.surveyForm = new FormGroup({
      surveyTitle: new FormControl(surveyTitle, [Validators.required]),
      surveyQuestions: surveyQuestions,
    });

    this.onAddQuestion();
  }

  onAddQuestion() {
    // console.log(this.surveyForm);
    const surveyQuestionItem = new FormGroup({
      questionTitle: new FormControl('', Validators.required),
      questionType: new FormControl('', Validators.required),
      questionGroup: new FormGroup({}),
    });
    (<FormArray>this.surveyForm.get('surveyQuestions')).push(
      surveyQuestionItem
    );
  }

  onRemoveQuestion(index) {
    this.surveyForm.controls.surveyQuestions['controls'][
      index
    ].controls.questionGroup = new FormGroup({});
    this.surveyForm.controls.surveyQuestions['controls'][
      index
    ].controls.questionType = new FormControl({});
    (<FormArray>this.surveyForm.get('surveyQuestions')).removeAt(index);
    this.selectedOption.splice(index, 1);
    // console.log(this.surveyForm);
  }

  onSelectQuestionType(questionType, index) {
    if (questionType === 'Radio' || questionType === 'Checkbox') {
      this.addOptionControls(questionType, index);
    }
  }

  addOptionControls(questionType, index) {
    let options = new FormArray([]);
    this.surveyForm.controls.surveyQuestions['controls'][
      index
    ].controls.questionGroup.addControl('options', options);

    this.clearFormArray(
      <FormArray>(
        this.surveyForm.controls.surveyQuestions['controls'][index].controls
          .questionGroup.controls.options
      )
    );

    this.addOption(index);
    this.addOption(index);
  }

  clearFormArray(formArray: FormArray) {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  addOption(index) {
    const optionGroup = new FormGroup({
      optionText: new FormControl('', Validators.required),
    });
    (<FormArray>(
      this.surveyForm.controls.surveyQuestions['controls'][index].controls
        .questionGroup.controls.options
    )).push(optionGroup);
  }
  
  removeOption(questionIndex, itemIndex) {
    (<FormArray>(
      this.surveyForm.controls.surveyQuestions['controls'][questionIndex]
        .controls.questionGroup.controls.options
    )).removeAt(itemIndex);
  }

  postSurvey() {
    let formData = this.surveyForm.value;
    console.log(formData);
    let ID = 0;
    let Title = formData.surveyTitle;
    let Questions = [];
    let surveyQuestions = formData.surveyQuestions;
    let survey = new Survey(ID, Title, Questions);

    surveyQuestions.forEach((question, index, array) => {
      let questionItem = {
        ID: 0,
        Type: question.questionType,
        Text: question.questionTitle,
        options: [],
      };
      if (question.questionGroup.hasOwnProperty('options')) {
        question.questionGroup.options.forEach((option) => {
          let optionItem: Option = {
            ID: 0,
            OptionText: option.optionText,
            OptionColor: '',
          };
          questionItem.options.push(optionItem);
        });
      }
      survey.Question.push(questionItem);
    });
  }

  exportJSON() {
    let json = document.createElement('a');
    json.href = URL.createObjectURL(
      new Blob([JSON.stringify(this.surveyForm.value, null, 2)], {
        type: 'text/plain',
      })
    );
    json.setAttribute('download', 'data.json');
    document.body.appendChild(json);
    json.click();
    document.body.removeChild(json);
  }

  uploadFile(event) {
    this.selectFile = event.target.files[0];
    let fileReader = new FileReader();
    fileReader.readAsText(this.selectFile, 'UTF-8');
    fileReader.onload = () => {
      console.log(fileReader.result.toString());
      this.jsonObject = JSON.parse(fileReader.result.toString());
      console.log(this.jsonObject);
    };
    fileReader.onerror = (error) => {
      console.log(error);
    };
  }

  onSubmit() {
    console.log(this.surveyForm.value);
  }
}
