export class Survey{
    constructor( 
        public ID: number,
        public Title: string,
        public Question: Question[]
        ){}
}

export class Question{
    constructor( 
        public ID: number,
        public Type: string,
        public Text: string,
        public options: Option[],
        ){}
}

export class Option{
    constructor(
        public ID: number,
        public OptionText: string,
        public OptionColor: string,
    ){}
}

export interface QuestionType {
    value: string;
    viewValue: string;
}