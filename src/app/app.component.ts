import { Component } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Survey, SurveyQuestion, QuestionGroup } from './model'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  
  selectFile;
  jsonObject
  survey : Survey[];
  surveyQuestion: SurveyQuestion;
  questionGroup: QuestionGroup;
  form: FormGroup;

  ngOnInit(){
    let surveyTitle = '';
    let surveyQuestions = new FormArray([]);
    this.form = new FormGroup({
      surveyTitle: new FormControl(surveyTitle, [Validators.required]),
      surveyQuestions: surveyQuestions,
    });
  }

  uploadFile(event) {
    this.selectFile = event.target.files[0];
    let fileReader = new FileReader();
    fileReader.readAsText(this.selectFile, 'UTF-8');
    fileReader.onload = () => {
      console.log(fileReader.result.toString());
      this.jsonObject = JSON.parse(fileReader.result.toString());
      console.log(this.jsonObject);
      this.survey = this.jsonObject;
      console.log(this.survey)
    };
    fileReader.onerror = (error) => {
      console.log(error);
    };
   
  }
}
